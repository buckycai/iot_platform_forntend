// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
    // 设备信息，数据表格
    'POST /api/v1/seniorFind/app':{
        "COUNT": 3,
        "RESULT": [
            {
                "id": "ba462d866a62efb1016a62f1a3fc0006",
                "create_time": 1556438164475,
                "create_user_id": null,
                "update_time": 1556438164475,
                "update_user_id": null,
                "app_name": "基础应用",
                "fk_app_type_id": null,
                "remark": null
            },
            {
                "id": "ba462d866a62fb3a016a62fc17a20000",
                "create_time": 1556438849429,
                "create_user_id": null,
                "update_time": 1556438849429,
                "update_user_id": null,
                "app_name": "高级应用",
                "fk_app_type_id": null,
                "remark": null
            },
            {
                "id": "ba462dc16a573d6a016a573e525a0000",
                "create_time": null,
                "create_user_id": null,
                "update_time": 1556241867910,
                "update_user_id": null,
                "app_name": "重量传感器",
                "fk_app_type_id": null,
                "remark": null
            }
        ]
    },
  };
  