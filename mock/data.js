// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
    // 设备信息，数据表格
    'POST /api/v1/seniorFind/device':{
        "count": 23,
        "RESULT": [
            {
                "id": 3,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "人脸识别摄像头",
                "device_remark": null,
                "fk_device_group_id": 3,
                "fk_device_type_id": 3
            },
            {
                "id": 4,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "RFID",
                "device_remark": null,
                "fk_device_group_id": 4,
                "fk_device_type_id": 4
            },
            {
                "id": 5,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "平板控制器",
                "device_remark": null,
                "fk_device_group_id": 5,
                "fk_device_type_id": 5
            },
            {
                "id": 6,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "安全锁",
                "device_remark": null,
                "fk_device_group_id": 6,
                "fk_device_type_id": 6
            },
            {
                "id": 7,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "无人车",
                "device_remark": null,
                "fk_device_group_id": 7,
                "fk_device_type_id": 7
            },
            {
                "id": 8,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "智能电子秤",
                "device_remark": null,
                "fk_device_group_id": 8,
                "fk_device_type_id": 8
            },
            {
                "id": 9,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "温度计2",
                "device_remark": null,
                "fk_device_group_id": 1,
                "fk_device_type_id": 1
            },
            {
                "id": 10,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "景深相机2",
                "device_remark": null,
                "fk_device_group_id": 2,
                "fk_device_type_id": 2
            },
            {
                "id": 11,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "人脸识别摄像头2",
                "device_remark": null,
                "fk_device_group_id": 3,
                "fk_device_type_id": 3
            },
            {
                "id": 12,
                "create_time": null,
                "create_user_id": null,
                "update_time": null,
                "update_user_id": null,
                "device_interval_time": null,
                "device_name": "RFID",
                "device_remark": null,
                "fk_device_group_id": 4,
                "fk_device_type_id": 4
            }
        ]
    },
  };
  