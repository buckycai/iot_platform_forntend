// 代码中会兼容本地 service mock 以及部署站点的静态数据
export default {
    // 模拟登陆
    'POST /subscription/getSubscribe':{
        "state": true,
        "data": [
            {
                "createUserId": null,
                "fk_device_type_id": null,
                "updateUserId": null,
                "updateTime": 1556208997658,
                "title": "测试2",
                "chosen": false,
                "device_interval_time": null,
                "device_name": "测试2",
                "createTime": null,
                "id": "4028c0816a5546b0016a5547beec0000",
                "fk_device_group_id": null,
                "key": "4028c0816a5546b0016a5547beec0000",
                "device_remark": null
            },
            {
                "createUserId": null,
                "fk_device_type_id": null,
                "updateUserId": null,
                "updateTime": 1556211034355,
                "title": "测试33",
                "chosen": true,
                "device_interval_time": null,
                "device_name": "测试33",
                "createTime": null,
                "id": "4028c0816a556024016a5560bc4a0000",
                "fk_device_group_id": null,
                "key": "4028c0816a556024016a5560bc4a0000",
                "device_remark": null
            },
            {
                "createUserId": null,
                "fk_device_type_id": null,
                "updateUserId": null,
                "updateTime": 1556211070937,
                "title": "测试34",
                "chosen": false,
                "device_interval_time": null,
                "device_name": "测试34",
                "createTime": null,
                "id": "4028c0816a5567ac016a556824400000",
                "fk_device_group_id": null,
                "key": "4028c0816a5567ac016a556824400000",
                "device_remark": null
            },
            {
                "createUserId": null,
                "fk_device_type_id": null,
                "updateUserId": null,
                "updateTime": 1556211288897,
                "title": "测试111",
                "chosen": true,
                "device_interval_time": null,
                "device_name": "测试111",
                "createTime": null,
                "id": "4028c0816a556aff016a556b53140000",
                "fk_device_group_id": null,
                "key": "4028c0816a556aff016a556b53140000",
                "device_remark": null
            },
            {
                "createUserId": null,
                "fk_device_type_id": null,
                "updateUserId": null,
                "updateTime": 1556211709736,
                "title": "12212112",
                "chosen": true,
                "device_interval_time": null,
                "device_name": "12212112",
                "createTime": null,
                "id": "4028c0816a55718e016a5571d9580000",
                "fk_device_group_id": null,
                "key": "4028c0816a55718e016a5571d9580000",
                "device_remark": null
            }
        ],
        "msg": ""
    },
  };
  