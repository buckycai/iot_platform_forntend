import { retrieval } from '@/services/requestData';

export default {
  namespace: 'device_find',

  state: {
    findType :[],
    findGroup :[],
  },

  effects: {
    *findType({ payload }, { call, put }) {
      const response = yield call(retrieval, payload);
      yield put({
        type: 'stateWithUpload',
        payload: {
          findType: Array.isArray(response) ? response : [],
        },
      });
    },
    *findGroup({ payload }, { call, put }) {
      const response = yield call(retrieval, payload);
      yield put({
        type: 'stateWithUpload',
        payload: {
          findGroup: Array.isArray(response) ? response : [],
        },
      });
    },
  },

  reducers: {
    stateWithUpload(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
