import { insert } from '@/services/requestData';
import { isFunction } from 'util';

export default {
    namespace: 'device_add',

    state: {
        response: null,
        id:null,
        device_name: '',
        fk_device_type_id: '',
        fk_device_group_id: '',
        device_remark: '',
    },

    effects: {
        *addDevice({ payload, callback  }, { call, put }) {
            const response = yield call(insert, payload);
            isFunction(callback) && callback(response);
            yield put({
                type: 'stateWithUpload',
                payload: {
                    response: response
                }
            });
        }
    },

    reducers: {
        stateWithUpload(state, action) {
            return {
                ...state,
                ...action.payload
            };
        }
    }
};