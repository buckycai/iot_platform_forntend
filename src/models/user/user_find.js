import { retrieval } from '@/services/requestData';

export default {
  namespace: 'user_find',

  state: {
    findType :[],
    findGroup :[],
  },

  effects: {
    *findUser({ payload }, { call, put }) {
      const response = yield call(retrieval, payload);
      yield put({
        type: 'stateWithUpload',
        payload: {
          findUser: Array.isArray(response) ? response : [],
        },
      });
    },
  },

  reducers: {
    stateWithUpload(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
