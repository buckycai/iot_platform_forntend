import { advanceSearch, distinct } from '@/services/requestData';
import { isFunction } from 'util';

export default {
  namespace: 'search',
  state: {},
  effects: {
    *componentSelects({ payload }, { call, put }) {
      const response = yield call(distinct, payload);
      let cache = {};
      if (Array.isArray(response)) {
        for (let key of response) {
          cache[key] = key;
        }
      }
      yield put({
        type: 'querySearch',
        payload: {
          [payload.column]: cache,
        },
      });
    },
    *searchData({ payload, callback }, { call }) {
      const response = yield call(advanceSearch, payload);
      isFunction(callback) && callback(response);
    },
  },
  reducers: {
    querySearch(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
