import { insert } from '@/services/requestData';
import { isFunction } from 'util';

export default {
    namespace: 'appType_add',

    state: {
        response: null,
        id:null,
        app_type_name: '',
    },

    effects: {
        *addAppType({ payload, callback  }, { call, put }) {
            const response = yield call(insert, payload);
            isFunction(callback) && callback(response);
            yield put({
                type: 'stateWithUpload',
                payload: {
                    response: response
                }
            });
        }
    },

    reducers: {
        stateWithUpload(state, action) {
            return {
                ...state,
                ...action.payload
            };
        }
    }
};