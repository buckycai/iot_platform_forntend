import { insert } from '@/services/requestData';
import { isFunction } from 'util';

export default {
    namespace: 'app_add',

    state: {
        response: null,
        id:null,
        app_name: '',
        remark: '',
    },

    effects: {
        *addApp({ payload, callback  }, { call, put }) {
            const response = yield call(insert, payload);
            isFunction(callback) && callback(response);
            yield put({
                type: 'stateWithUpload',
                payload: {
                    response: response
                }
            });
        }
    },

    reducers: {
        stateWithUpload(state, action) {
            return {
                ...state,
                ...action.payload
            };
        }
    }
};