import { retrieval } from '@/services/requestData';

export default {
  namespace: 'app_subscribe',

  state: {
    findSub :[],
  },

  effects: {
    *findSub({ payload }, { call, put }) {
      const response = yield call(retrieval, payload);
      yield put({
        type: 'stateWithUpload',
        payload: {
            findSub: Array.isArray(response) ? response : [],
        },
      });
    },
  },

  reducers: {
    stateWithUpload(state, action) {
      return {
        ...state,
        ...action.payload,
      };
    },
  },
};
