import request from '@/utils/request';
import { async } from 'q';
import { downloadExcel, downloadZip, openNotificationWithIcon } from '@/utils/tools'
import $ from 'jquery';

/**
 * 插入数据
 *
 * @author dsy 2019-03-06
 * @param {str} params.datatable 数据表名
 * @param {*} params.body 用于提交的数据
 */
export async function insert(params) {
  return request(`/api/v1/save/${params.datatable}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: params.body,
  });
}

/**
 * 找出数据表中不同的数据条目
 *
 * @author dsy 2019-03-07
 * @param {str} params.datatable 数据表名
 * @param {str} params.column 数据表列名
 */
export async function distinct(params) {
  return request(`/api/v1/queryDiff/${params.datatable}/${params.column}`);
}

/**
 * 统计表中有多少条数据
 *
 * @author LongTeng 2019-03-09
 * @param {str} params
 */
export async function count(params) {
  return request(`/api/v1/queryDiff/${params.datatable}`);
}

/**
 * 通过ID删除数据
 *
 * @author LongTeng 2019-03-09
 * @param {str} params
 */
export async function deleteById(tableName,id) {
  return request(`/api/v1/delete/${tableName}/${id}`);
}

/**
 * 检索数据
 *
 * @author dsy 2019-03-06
 * @param {str} params.datatable 数据表名
 * @param {arr} params.conds 检索字段
 */
export async function retrieval(params) {
  let conds = [];
  if (Array.isArray(params.conds)) {
    conds = params.conds;
  }
  return request(`/api/v1/find/${params.datatable}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: conds,
  });
}

/**
 * 高级检索（参数超级多）
 *
 * @author dsy 2019-03-08
 * @param {str} params.datatable 主表数据表名
 * @param {arr} params.conditions 查询条件
 * @param {arr} params.relations 外键关系
 * @param {num} params.pageNum 页码
 * @param {num} params.pageSize 页大小
 * @param {str} params.order.field 排序字段名
 * @param {boolean} params.order.mode true升序、false降序
 */
export async function advanceSearch(params) {
  let restrict = '';
  if (params.pageNum > 0 && params.pageSize > 0) {
    restrict = `?pageNum=${params.pageNum}&pageSize=${params.pageSize}`;
  }
  return request(`/api/v1/seniorFind/${params.datatable}${restrict}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: {
      conditions: params.conditions,
      relations: params.relations,
      order: params.order,
    },
  });
}

/**
 * 查询出的数据条目下载（超多参数控制条目下载）
 *
 * @author dsy 2019-03-11
 * @param {str} handle.datatable 主表数据表名
 * @param {arr} handle.conditions 查询条件
 * @param {arr} handle.relations 外键关系
 * @param {str} handle.order.field 排序字段名
 * @param {boolean} handle.order.mode true升序、false降序
 * @param {boolean} handle.isAll 全选标志
 * @param {arr} handle.rows 勾选行的ID列表
 * @param {arr} handle.cols 筛选列
 * @param {boolean} showCol 是否显示ID列
 */
export async function downloadLines(handle, showCol) {
  let conds = showCol? "?flag=true": "";
  let body = {
    conditions: handle.conditions,
    relations: handle.relations,
    order: handle.order,
    isAll: handle.selectAll,
    rows: handle.selectedIndics,
    cols: handle.selectedCols,
  };
  return await request(`/api/theme/download/${handle.dataTable}${conds}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: body,
  }, true);
}

/**
 * 从后端获取PDF文件流，推送到前端
 *
 * @author LongTeng 2019-03-11
 */
export async function getPDF(pdfName) {
  return await request(`/api/theme/downloadPDF?name=${pdfName}`, {
    method: 'GET',
    headers: {
      'Content-Type': 'application/json',
    },
  }, true);
}

/**
 * 从后端获取Excel文件流，推送到前端，提供下载
 *
 * @author LongTeng 2019-03-26
 */
export async function getDownloadExcel(URL) {
  return await request(`${URL}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  }, true);
}

/**
 * 获取应用订阅信息，返回该应用对应所有设备，是否订阅的数组
 * 
 * @author LongTeng 2019-04-26
 * @param {str} key 
 * @param {str} entity 
 */
export async function getSubscribe(key,entity) {
  return await request(`/subscription/getSubscribe/${key}/${entity}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

/**
 * 查询某个设备（或 设备类型 或 设备组）被哪些app订阅
 * 
 * @author LongTeng 2019-04-29
 * @param {str} key 
 * @param {str} entity 
 */
export async function getSubscribeBy(key,entity) {
  return await request(`/subscription/getSubscribeBy/${key}/${entity}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
  });
}

/**
 * 订阅消息，通过传入设备id，订阅类型，订阅设备数组，来订阅消息
 * 
 * @author LongTeng 2019-04-28
 * @param {object} param 
 */
export async function subscribe(param) {
  let body = {};
  body[param.Bodys]=param.Keys;
  return await request(`/subscription/subscribe/${param.id}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: body,
  }, true);
}

/**
 * 获得当前登陆用户的所有信息
 * 
 *  @author LongTeng 2019-04-29
 */
export async function getLoginUserInfo() {
  return request(`/system/getLoginUserInfo`);
}

/**
 * 获得权限
 * 
 * @author dsy 2019-03-27
 */
export function getPower() {
  let authority = "U";

  $.ajax({
    url: "/system/getLoginUserInfo",
    method: 'GET',
    processData: false,
    async: false,
    beforeSend: (xhr) => {
      xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
      xhr.setRequestHeader('Accept', 'application/json');
    },
    success: (data) => {
      authority = data.auths;
    }
  });

  return authority;
}
