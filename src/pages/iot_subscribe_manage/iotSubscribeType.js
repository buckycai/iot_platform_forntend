import React, { Component, PureComponent } from 'react';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import IotDeviceForm from '@/components/IotDeviceForm';
import { Card, Divider, Button, Icon, Modal,Form } from 'antd';
import AdvancedSearch from '@/components/AdvancedSearch';
import AdvancedTable from '@/components/AdvancedTable';
import styles from './iotSubscribe.less';
import { deleteById, retrieval} from '@/services/requestData';
import { openNotificationWithIcon, switcher } from '@/utils/tools';

/**
 * 订阅列表
 * @author LongTeng 2019-04-25
 */

export default class IotSubscribe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datalines: [],
      loading: false,
      visible: false,
      row: {},

      columnTypeKey:[],
      columnTypeValue:[],
      columnGroupKey:[],
      columnGroupValue:[],
    };
    // 高级检索配置
    this.master = 'subscriptiondevicetype';
    this.items = [
      { key: "fk_app_id", name: "应用名称", type: "search_combo", first: true, datatable: this.master,async:
      retrieval({datatable:"app",conds:[]}).then(res=>{
        let dicType = {};
        res.forEach(x=>{
          dicType[x.app_name]=x.id;
        })
          return dicType;
      }).catch(e=>{
        openNotificationWithIcon("error","下拉列表读取失败","数据读取失败，请确认网络连接是否正常");
      })
    },
      { key: "fk_device_type_id", name: "订阅设备类型", type: "search_combo", first: true, datatable: this.master,async:
      retrieval({datatable:"devicetype",conds:[]}).then(res=>{
        let dicType = {};
        res.forEach(x=>{
          dicType[x.device_type_name]=x.id;
        })
          return dicType;
      }).catch(e=>{
        openNotificationWithIcon("error","下拉列表读取失败","数据读取失败，请确认网络连接是否正常");
      })
    },
    ];
    // 表格栏目对应字段配置
    this.columns = [
      { title: '应用名称', dataIndex: 'fk_app_id', sorter: true, first: true , render: (val) => {
        return switcher(val, this.state.columnTypeKey, this.state.columnTypeValue, "应用不存在");
      } },
      { title: '订阅设备类型', dataIndex: 'fk_device_type_id', sorter: true, first: true , render: (val) => {
        return switcher(val, this.state.columnGroupKey, this.state.columnGroupValue, "暂无订阅");
      } },
      { title: '操作', dataIndex: '$$operations', render: (row) => {
        return (<div>
          <Button className={styles.buttonDelete} onClick={() => {
              Modal.confirm({
                title: '删除',
                content: '确定要删除该数据吗？',
                onOk() {
                  let response = deleteById("Device",row.id);
                  response.then(content => {
                    openNotificationWithIcon("success","删除成功","恭喜，删除成功！");
                    let handle = row.getHandle();
                    handle.updateList();
                  }).catch(e=>{
                    openNotificationWithIcon("error","加载失败","无法获得数据，请检查信息是否正确");
                  });
                },
                onCancel() {},
              });
          }}>删除</Button>
          </div>);
      } },
    ];
    // 特殊控制按钮配置
    this.controllers = [];
  }
  
  componentDidMount(){
    let responseType = retrieval({datatable:"app",conds:[]});
    responseType.then(res=>{
      let columnTypeKey=[];
      let columnTypeValue=[];
      res.forEach(x=>{
        columnTypeKey.push(x.id);
        columnTypeValue.push(x.app_name);
      })
      this.setState({
        columnTypeKey:columnTypeKey,
        columnTypeValue:columnTypeValue,
      })      
    }).catch(e=>{
      openNotificationWithIcon("error","数据读取失败","数据读取失败，请确认网络连接是否正常");
    })

    let responseGroup = retrieval({datatable:"device",conds:[]})
    responseGroup.then(res=>{
      let columnGroupKey=[];
      let columnGroupValue=[];
      res.forEach(x=>{
        columnGroupKey.push(x.id);
        columnGroupValue.push(x.device_name);
      })
      this.setState({
        columnGroupKey:columnGroupKey,
        columnGroupValue:columnGroupValue,
      })   
    }).catch(e=>{
      openNotificationWithIcon("error","数据读取失败","数据读取失败，请确认网络连接是否正常");
    })
  }


  render() {
    return (
      <PageHeaderWrapper>
        <Card bordered={true}>
          <AdvancedTable
            columns={this.columns}
            bordered
            // useMultiSelect
            updateFirst
            controllers={this.controllers}
            master={this.master}
            items={this.items}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}
