import React, { Component, PureComponent } from 'react';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import IotDeviceForm from '@/components/IotDeviceForm';
import { Card, Divider, Button, Icon, Modal,Form } from 'antd';
import AdvancedSearch from '@/components/AdvancedSearch';
import AdvancedTable from '@/components/AdvancedTable';
import moment from 'moment';
import styles from './iotMessage.less';
import { deleteById, retrieval} from '@/services/requestData';
import { openNotificationWithIcon, switcher } from '@/utils/tools';

/**
 * 数据查看列表页面
 * @author Nyy 2019-04-19
 */

export default class IotMessage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datalines: [],
      loading: false,
      visible: false,
      row: {},

      refreshTime: 0,
      
      columnNameKey:[],
      columnNameValue:[],
      
    };
    // 高级检索配置
    this.master = 'devicedata';
    this.items = [
      { key: "fk_device_id", name: "设备名称", type: "search_combo", first: true, datatable: this.master,async:
      retrieval({datatable:"device",conds:[]}).then(res=>{
        let dicType = {};
        res.forEach(x=>{
          dicType[x.device_name]=x.id;
        })
          return dicType;
      }).catch(e=>{
        openNotificationWithIcon("error","下拉列表读取失败","数据读取失败，请确认网络连接是否正常");
      })
    },
    ];
    // 表格栏目对应字段配置
    this.columns = [
      { title: '设备名称', dataIndex: 'fk_device_id', sorter: true, first: true , render: (val) => {
        return switcher(val, this.state.columnNameKey, this.state.columnNameValue, "设备名不存在");
      } },
      { title: '接收时间', dataIndex: 'create_time', sorter: true, first: true, render:(time)=>{
        return moment(parseInt(time)).format("LLL");
      }},
      { title: '修改时间', dataIndex: 'update_time', sorter: true, first: true, render:(time)=>{
        return moment(parseInt(time)).format("LLL");
      }},
      { title: '接收数据', dataIndex: 'data_content', sorter: true, first: true },
      { title: '下载数据', dataIndex: 'data_url', sorter: true, first: true, render:(url)=>{
        if(url){
          return <Button type="primary"><a onClick={window.location.href=url} >下载</a></Button>
        }else{
          return "";
        }
      }},
    ];
    // 特殊控制按钮配置
    this.controllers = [];
  }
  
  //默认render结束后，从数据库获取数据，替换表格中显示的id，默认显示对应id的中文名称
  componentDidMount(){
    let responseName = retrieval({datatable:"device",conds:[]});
    responseName.then(res=>{
      let columnNameKey=[];
      let columnNameValue=[];
      res.forEach(x=>{
        columnNameKey.push(x.id);
        columnNameValue.push(x.device_name);
      })
      this.setState({
        columnNameKey:columnNameKey,
        columnNameValue:columnNameValue,
      })      
    }).catch(e=>{
      openNotificationWithIcon("error","数据读取失败","数据读取失败，请确认网络连接是否正常");
    })

    setInterval(() => {
      let refreshTime = this.state.refreshTime;
      console.log("刷新次数："+refreshTime+1);
      this.setState({refreshTime:refreshTime+1});
    }, 5000);
  }

 
  render() {
    return (
      <PageHeaderWrapper>
        <div style={{display:"none"}}>{this.state.refreshTime}</div>
        <Card bordered={true}>
          <AdvancedTable
            columns={this.columns}
            bordered
            // useMultiSelect
            updateFirst
            controllers={this.controllers}
            master={this.master}
            items={this.items}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}
