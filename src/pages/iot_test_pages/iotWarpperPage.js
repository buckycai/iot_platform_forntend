import React, { Component } from 'react';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import IotSubscribeWarpper from '@/components/IotSubscribeWarpper';
import styles from './iotWarpperPage.less';

/**
 * 设备订阅穿梭框测试页面
 * @author LongTeng 2019-04-25
 */


export default class IotWarpperPage extends Component {
    render() {
        return (
            <PageHeaderWrapper>
                <IotSubscribeWarpper />
            </PageHeaderWrapper>
        );
    }
}
