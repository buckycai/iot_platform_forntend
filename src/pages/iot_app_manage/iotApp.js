import React, { Component, PureComponent } from 'react';
import { connect } from 'dva';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import IotAppForm from '@/components/IotAppForm';
import IotSubscribeWarpper from '@/components/IotSubscribeWarpper';
import { Card, Divider, Button, Icon, Modal,Form } from 'antd';
import AdvancedSearch from '@/components/AdvancedSearch';
import AdvancedTable from '@/components/AdvancedTable';
import styles from './iotApp.less';
import { deleteById, retrieval, subscribe } from '@/services/requestData';
import { openNotificationWithIcon, QueryCond, deleteInvalidSigns } from '@/utils/tools';

/**
 * 应用列表
 * @author Bucky 2019-04-22
 */
@connect(({ app_add, loading }) => ({
  app_add,
  loading: loading.effects['models/app/app_add'],
}))
export default class IotApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      datalines: [],
      loading: false,
      visible: false,
      visibleSub:false,
      row: {},
    };
    // 高级检索配置
    this.master = 'app';
    this.items = [
      {  key: 'app_name',  name: '应用名称',  type: 'input',  first: true,  datatable: this.master,  mode: 3,},
    ];
    // 表格栏目对应字段配置
    this.columns = [
      { title: '应用编号', dataIndex: 'id', sorter: true, first: true },
      { title: '应用名称', dataIndex: 'app_name', sorter: true, first: true },
      { title: '应用备注', dataIndex: 'remark', sorter: true, first: true },
      { title: '操作', dataIndex: '$$operations', render: (row) => {
        return (<div>
          <Button className={styles.buttonCheck} onClick={() => {
              console.log(row);
              this.setState({
                  row:row,
                  visible: true,
                  disabled:true,
              });
          }}>查看</Button>
          <Button className={styles.buttonEdit} onClick={() => {
              let handle = row.getHandle();
              this.setState({
                  row:row,
                  visible: true,
                  disabled:false,
                  handle:handle,
              });
              handle.updateList();
          }}>修改</Button>
          <Button className={styles.buttonDelete} onClick={() => {
              Modal.confirm({
                title: '删除',
                content: '确定要删除该数据吗？',
                onOk() {
                  let response = deleteById("App",row.id);
                  response.then(content => {
                    openNotificationWithIcon("success","删除成功","恭喜，删除成功！");
                    let handle = row.getHandle();
                    handle.updateList();
                  }).catch(e=>{
                    openNotificationWithIcon("error","加载失败","无法获得数据，请检查信息是否正确");
                  });
                },
                onCancel() {},
              });
          }}>删除</Button>
          <Button className={styles.buttonSubscribe} onClick={() => {
              console.log(row);
              this.setState({
                  row:row,
                  visibleSub: true,
              });
          }}>订阅</Button>
          </div>);
      } },
    ];
    // 特殊控制按钮配置
    this.controllers = [
      {
        name: '添加',
        icon: 'file',
        callback: handle => {
          this.setState({
            row: {},
            visible: true,
            disabled: false,
            handle: handle,
          });
          handle.updateList();
        },
      },
    ];
  }

  //提交信息，通过子组件传出的值，组装数据，向后端提交应用信息
  handleSubmit = (err, values) => {
    const { dispatch } = this.props;
    if (!err) {
      // let conds = new QueryCond();
      // conds.exactQuery('app_name', deleteInvalidSigns(values.app_name));
      // let searchParam = {datatable:"app",conds:conds.getConds()};
      // let response = retrieval(searchParam)
      // response.then(e=>{
      //   if(e.length>0){
      //     openNotificationWithIcon("error","提交失败","提交失败，列表中已存在该设备类型！");
      //   }else{
          let form = {};
          let objKeys = Object.keys(values);
          for(var one of objKeys){
            form[one] = values[one];
          }
          form.id = this.state.row.id;
          console.log(form);
          dispatch({
            type: 'app_add/addApp',
            payload: {
              datatable: "App",
              body: form,
            },
            callback:()=>{
              let handle = this.state.handle;
              handle && handle.updateList();
            }
          });
          this.setState({visible: false})
      //   }
      // }).catch(e=>{
      //   openNotificationWithIcon("error","提交失败","请检查填写是否正确");
      // })
    }
  };

  //弹出提交成功与否的信息（此方法通过dva的回调使用）
  showAlert = () => {
    const duration = 3.5;
    if (this.props.app_add.response) {
      let { state } = this.props.app_add.response;
      if (state) {
        openNotificationWithIcon("success","提交成功","恭喜，提交成功！");
      } else {
        openNotificationWithIcon("error","提交失败","列表中已存在该设备类型！");
      }
      this.props.app_add.response = null;
    }
  }

  //查看、修改、添加数据弹窗
  showWindow = () => {
    const IotAppFormWrapper = Form.create({ name: 'IotAppForm' })(IotAppForm);
    this.showAlert();
    return (
      <Modal
        visible={this.state.visible}
        title="详细"
        width="800px"
        destroyOnClose
        onCancel={() => {
          this.setState({ visible: false });
        }}
        footer={null}
      >
        <div>
          <Card className={styles.selectCard}>
            <IotAppFormWrapper  onSubmit={this.handleSubmit}  disabled={this.state.disabled}  result={this.state.row}/>
          </Card>
        </div>
      </Modal>
    );
  };

  //点击订阅显示订阅弹窗
  showSubscribe = () => {
    return (
      <Modal
        visible={this.state.visibleSub}
        title="详细"
        width="1000px"
        destroyOnClose
        onCancel={() => {
          this.setState({ visibleSub: false });
        }}
        footer={null}
      >
        <div>
          <Card className={styles.selectCard}>
            <IotSubscribeWarpper  subSubmit={this.subSubmit}  result={this.state.row}/>
          </Card>
        </div>
      </Modal>
    );
  };

  //提交方法，通过子组件传出的值，组装数据，向后端提交订阅信息
  subSubmit = (Ids, Keys, Bodys) =>{
    let param = {};
    param["id"]=Ids;
    param["Bodys"]=Bodys;
    param["Keys"] = Keys;
    let response = subscribe(param);
    response.then(res=>{
      openNotificationWithIcon('success', '订阅成功', '恭喜，订阅成功！');
      this.setState({ visibleSub: false });
    }).catch(e=>{
      openNotificationWithIcon('error', '订阅失败', '订阅失败，请检查网络连接是否正常！');
      this.setState({ visibleSub: false });
    })
  }

  render() {
    return (
      <PageHeaderWrapper>
        {this.showWindow()}
        {this.showSubscribe()}
        <Card bordered={true}>
          <AdvancedTable
            columns={this.columns}
            bordered
            // useMultiSelect
            updateFirst
            controllers={this.controllers}
            master={this.master}
            items={this.items}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}
