import {getPower} from '@/services/requestData';

export function getAuthority(str) {
  return getPower();
}
export function setAuthority(authority) {
  const proAuthority = typeof authority === 'string' ? [authority] : authority;
  return localStorage.setItem('shou-authority', JSON.stringify(proAuthority));
}
