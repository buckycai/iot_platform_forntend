import react from 'react';
import { connect } from 'dva';
import { Component } from 'react';
import { isFunction } from 'util';
import { insert } from '@/services/requestData';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, notification} from 'antd';
import styles from './index.less';
import {openNotificationWithIcon} from '@/utils/tools';

/**
 * 设备管理表单提交组件
 * @author Bucky 2019-04-20
 */

 //控制表单样式
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const { Option } = Select;

//dva
@connect(({ device_find, loading }) => ({
  device_find,
  loading: loading.effects['models/device/device_find'],
}))


export default class IotDeviceForm extends Component {
  constructor() {
    super();
  }

  //通过dva获取数据
  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'device_find/findType',
      payload: {
        datatable: 'devicetype',
        conds:[],
      },
    },);
    dispatch({
      type: 'device_find/findGroup',
      payload: {
        datatable: 'devicegroup',
        conds:[],
      },
    },);
  }

  //提交按钮
  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (isFunction(this.props.onSubmit)) {
        this.props.onSubmit(err, values);
      }
    });
  };

  render() {
    let result = this.props.result;
    const { getFieldDecorator } = this.props.form;
    const { TextArea } = Input;

    this.optionsType = [];
    this.optionsGroup = [];

    this.typeDefault  = {key: "", label: ""};
    this.groupDefault = {key: "", label: ""};

    //利用dva获取的数据，将选项添加到下拉框中
    for (var one of this.props.device_find.findType) {
      if(result.fk_device_type_id == one.id){
        this.typeDefault.key = result.fk_device_type_id;
        this.typeDefault.label = one.device_type_name
      }
      this.optionsType.push(<Option key={one.id} >{one.device_type_name}</Option>);
    }
    for (var one of this.props.device_find.findGroup) {
      if(result.fk_device_group_id == one.id){
        this.groupDefault.key = result.fk_device_group_id;
        this.groupDefault.label = one.device_group_name
      }
      this.optionsGroup.push(<Option key={one.id} >{one.device_group_name}</Option>);
    }

    return (
      <Form className={styles.mainForm} {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item {...formItemLayout} label="id：">
          {getFieldDecorator('id', {
            initialValue: result.id,
            rules: [{ required: false}],
          })(<Input disabled="true"/>)}
        </Form.Item>
        <Form.Item label="设备名称">
          {getFieldDecorator('device_name', {
            initialValue: result.device_name,
            rules: [{ required: true, message: '请输入设备名称！' }],
          })(<Input disabled={!!this.props.disabled}/>)}
        </Form.Item>
        <Form.Item label="设备类型">
          {getFieldDecorator('fk_device_type_id', {
            initialValue: this.typeDefault,
            rules: [{ required: true, message: '请输入设备类型！' }],
          })(<Select showSearch labelInValue disabled={!!this.props.disabled}>{this.optionsType}</Select>)}
        </Form.Item>
        <Form.Item label="设备组">
          {getFieldDecorator('fk_device_group_id', {
            initialValue: this.groupDefault,
            rules: [{ required: false}],
          })(<Select showSearch labelInValue disabled={!!this.props.disabled}>{this.optionsGroup}</Select>)}
        </Form.Item>
        <Form.Item label="设备备注">
          {getFieldDecorator('device_remark', {
            initialValue: result.device_remark,
            rules: [{ required: false}],
          })(<TextArea disabled={!!this.props.disabled} rows={8}/>)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" disabled={!!this.props.disabled}>
            提交
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
