import React, { Component } from 'react';
import { Transfer, Button } from 'antd';
import styles from './index.less';
import { getSubscribe } from '@/services/requestData';
import { openNotificationWithIcon } from '@/utils/tools';
import { isFunction } from 'util';

/**
 * 设备订阅卡片穿梭框
 * @author LongTeng 2019-04-25
 */

export default class IotSubscribeDevice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mockData: [],
      targetKeys: [],
      selectedKeys: [],
      disabled: false,
    };
  }

  componentDidMount() {
    let targetKeys = [];
    let mockData = [];
    //通过父组件传入的应用id、设备实体类名，从后端获取该应用对于所有设备的订阅信息，并绑定到穿梭框中
    let response = getSubscribe(this.props.resId, this.props.resEntity);
    response.then(res => {
        for (let one of res.data) {
          if (one.chosen) {
            targetKeys.push(one.key);
          }
          mockData.push(one);
        }
        this.setState({ mockData, targetKeys });
      })
      .catch(e => {
        openNotificationWithIcon('error', '数据读取失败', '数据读取失败，请确认网络连接是否正常');
      });
  }

  handleChange = (nextTargetKeys, direction, moveKeys) => {
    //点击箭头时，更新穿梭框右部的数组值，数组内容为设备id
    this.setState({ targetKeys: nextTargetKeys });
  };

  handleSelectChange = (sourceSelectedKeys, targetSelectedKeys) => {
    this.setState({ selectedKeys: [...sourceSelectedKeys, ...targetSelectedKeys] });
  };

  //回调方法，把子组件传出来的值，再推送到父组件的提交方法中
  handleSubmit = () => {
    //判断父组件传入的实体类名，通过实体类名，定义需要传入后端接口的订阅类型的ID
    let subscribeBody = '';
    switch (this.props.resEntity) {
      case 'subscriptiondevice':
        subscribeBody = 'device_id';
        break;
      case 'subscriptiondevicetype':
        subscribeBody = 'device_type_id';
        break;
      case 'subscriptiondevicegroup':
        subscribeBody = 'device_group_id';
        break;
      default:
        openNotificationWithIcon('error', '订阅失败', '请选择订阅类型！');
        break;
    }

    //调用父组件的提交方法，将应用id、订阅设备数组、订阅类型标识
    if (isFunction(this.props.onSubmit)) {
      this.props.onSubmit(this.props.resId, this.state.targetKeys, subscribeBody);
    } else {
      openNotificationWithIcon('error', '订阅失败', '订阅失败，请检查勾选订阅设备是否有误！');
    }
  };

  render() {
    let searchHint = '';
    switch (this.props.resEntity) {
      case 'subscriptiondevice':
        searchHint = '请输入设备';
        break;
      case 'subscriptiondevicetype':
        searchHint = '请输入设备类型';
        break;
      case 'subscriptiondevicegroup':
        searchHint = '请输入设备组';
        break;
      default:
        searchHint = '请输入设备';
        break;
    }

    return (
      <div>
        <Transfer
          dataSource={this.state.mockData}
          showSearch
          targetKeys={this.state.targetKeys}
          onChange={this.handleChange}
          onSelectChange={this.handleSelectChange}
          render={item => item.title}
          titles={["未订阅","已订阅"]}
          locale={{ itemUnit: '项', itemsUnit: '项', notFoundContent: '列表为空', searchPlaceholder: searchHint }}
          listStyle={{
            width: "47%",
            height: "400px",
          }}
        />
        <Button type="primary" className={styles.submitButton} onClick={this.handleSubmit}>
          提交
        </Button>
      </div>
    );
  }
}
