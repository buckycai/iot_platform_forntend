import React, { Component } from 'react';
import styles from './index.less';
import { Table } from 'antd';
import { getSubscribeBy } from '@/services/requestData';
import { openNotificationWithIcon } from '@/utils/tools';

/**
 * 该页面用于显示具体某个设备被哪些应用所订阅，以表格的方式展示应用
 * 
 * @author LongTeng 2019-04-30
 */
export default class IotDeviceFind extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
    };
  }

  //通过接口传入设备id、实体类名，从后端拉取数据，填充到表格上
  componentDidMount() {
    let response = getSubscribeBy(this.props.result.id, this.props.resEntity);
    response
      .then(res => {
        this.setState({ data: res.data });
      })
      .catch(e => {
        openNotificationWithIcon('error', '加载失败', '获取数据失败，请检查网络连接是否正常！');
      });
  }

  render() {
    //表头配置
    const columns = [
      {
        title: '应用id',
        dataIndex: 'id',
        key: 'id',
      },
      {
        title: '应用名称',
        dataIndex: 'app_name',
        key: 'app_name',
      },
    ];

    return (
      <div>
        <Table columns={columns} dataSource={this.state.data} />
      </div>
    );
  }
}
