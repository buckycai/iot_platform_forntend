import react from 'react';
import { Component } from 'react';
import { isFunction } from 'util';
import { insert } from '@/services/requestData';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete,} from 'antd';
import styles from './index.less';
import {openNotificationWithIcon} from '@/utils/tools';

/**
 * 设备管理表单提交组件
 * @author Bucky 2019-04-20
 */

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

export default class IotDeviceType extends Component {
  constructor() {
    super();
  }

  handleSubmit = e => {
    let result = this.props.result;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (isFunction(this.props.onSubmit)) {
        values.id = result.id ;
        this.props.onSubmit(err, values);
      }
    });
  };

  render() {
    let result = this.props.result;
    const { getFieldDecorator } = this.props.form;
    return (
      <Form className={styles.mainForm} {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item style={{display:"none"}} {...formItemLayout} label="id：">
          {getFieldDecorator('id', {
            initialValue: result.id,
            rules: [{ required: false}],
          })(<Input disabled={!!this.props.disabled}/>)}
        </Form.Item>
        <Form.Item label="设备类型名称">
          {getFieldDecorator('device_type_name', {
            initialValue: result.device_type_name,
            rules: [{ required: true, message: '请输入设备类型名称！' }],
          })(<Input disabled={!!this.props.disabled}/>)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" disabled={!!this.props.disabled}>
            提交
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
