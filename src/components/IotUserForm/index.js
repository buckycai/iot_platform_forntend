import react from 'react';
import { Component } from 'react';
import { isFunction } from 'util';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, notification } from 'antd';
import styles from './index.less';
import { openNotificationWithIcon } from '@/utils/tools';

/**
 * 用户管理表单提交组件
 * @author Bucky 2019-04-24
 */

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

export default class IotUserForm extends Component {
  constructor() {
    super();
  }

  handleSubmit = e => {
    let result = this.props.result;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (isFunction(this.props.onSubmit)) {
        this.props.onSubmit(err, values);
      }
    });
  };

  render() {
    let result = this.props.result;
    const { getFieldDecorator } = this.props.form;
    const Option = Select.Option;
    let defaultAuth ={}
    switch (result.auths) {
      case 'S':
        defaultAuth["key"]='S';
        defaultAuth["label"]='超级管理员';
        break;
      case 'A':
        defaultAuth["key"]='A';
        defaultAuth["label"]='管理员';
        break;
      case 'U':
        defaultAuth["key"]='U';
        defaultAuth["label"]='普通用户';
        break;
      default:
        defaultAuth["key"]='';
        defaultAuth["label"]='';
        break;
    }
    return (
      <Form className={styles.mainForm} {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item style={{ display: "none" }} {...formItemLayout} label="id：">
          {getFieldDecorator('id', {
            initialValue: result.id,
            rules: [{ required: false }],
          })(<Input disabled={!!this.props.disabled} />)}
        </Form.Item>
        <Form.Item label="用户名称">
          {getFieldDecorator('username', {
            initialValue: result.username,
            rules: [{ required: true, message: '请输入用户名称！' }],
          })(<Input disabled={!!this.props.disabled} />)}
        </Form.Item>
        <Form.Item label="用户密码">
          {getFieldDecorator('password', {
            initialValue: result.password,
            rules: [{ required: true, message: '请输入用户密码！' }],
          })(<Input.Password disabled={!!this.props.disabled} />)}
        </Form.Item>
        <Form.Item label="用户权限">
          {getFieldDecorator('auths', {
            initialValue: defaultAuth,
            rules: [{ required: true, message: '请选择用户权限！' }],
          })(<Select labelInValue  disabled={!!this.props.disabled}>
            <Option key="S">超级管理员</Option>
            <Option key="A">管理员</Option>
            <Option key="U">普通用户</Option>
          </Select>)
          }
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" disabled={!!this.props.disabled}>
            提交
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
