import React, { Component } from 'react';
import { Tabs, Card, Transfer } from 'antd';
import styles from './index.less';
import IotSubscribeDevice from '@/components/IotSubscribeDevice';
import { isFunction } from 'util';
import { openNotificationWithIcon } from '@/utils/tools';

/**
 * 设备订阅卡片切换功能
 * @author LongTeng 2019-04-25
 */

const TabPane = Tabs.TabPane;

export default class IotSubscribeWarpper extends Component {
  constructor(props) {
    super(props);
  }

  //回调方法，把子组件传出来的值，再推送到父组件的提交方法中
  onSubmit = (Ids, Keys, Bodys) => {
    if (isFunction(this.props.subSubmit)) {
      this.props.subSubmit(Ids, Keys, Bodys);
    } else {
      openNotificationWithIcon('error', '订阅失败', '订阅失败，请检查勾选订阅设备是否有误！');
    }
  };

  render() {
    /**
     * 标签切换组件，切换不同类型的穿梭框
     * 通过传入实体类名、应用id，从而在子组件内通过后端拉取数据绑定在穿梭框中
     * @param {str} resEntity 实体类名
     * @param {str} resId 应用id
     */
    let result = this.props.result;
    return (
      <div>
        <div className={styles.title}>设备订阅</div>
        <Tabs defaultActiveKey="1" >
          <TabPane tab="设备订阅" key="1">
            <IotSubscribeDevice resEntity="subscriptiondevice" resId={result.id} onSubmit={this.onSubmit}/>
          </TabPane>
          <TabPane tab="设备类型订阅" key="2">
            <IotSubscribeDevice resEntity="subscriptiondevicetype" resId={result.id} onSubmit={this.onSubmit}/>
          </TabPane>
          <TabPane tab="设备组订阅" key="3">
            <IotSubscribeDevice resEntity="subscriptiondevicegroup" resId={result.id} onSubmit={this.onSubmit}/>
          </TabPane>
        </Tabs>
      </div>
    );
  }
}