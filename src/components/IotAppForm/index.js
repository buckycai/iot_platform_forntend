import react from 'react';
import { connect } from 'dva';
import { Component } from 'react';
import { isFunction } from 'util';
import { insert } from '@/services/requestData';
import { Form, Input, Tooltip, Icon, Cascader, Select, Row, Col, Checkbox, Button, AutoComplete, notification} from 'antd';
import styles from './index.less';
import {openNotificationWithIcon} from '@/utils/tools';

/**
 * 应用管理表单提交组件
 * @author NYY 2019-04-22
 */

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

@connect(({ app_find, loading }) => ({
  app_find,
  loading: loading.effects['models/app/app_find'],
}))

export default class IotAppForm extends Component {
  constructor() {
    super();
  }

  
  //通过dva获取数据
  componentDidMount = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'app_find/findType',
      payload: {
        datatable: 'apptype',
        conds:[],
      },
    },);
  }

  handleSubmit = e => {
    let result = this.props.result;
    e.preventDefault();
    this.props.form.validateFieldsAndScroll((err, values) => {
      if (isFunction(this.props.onSubmit)) {
        values.id = result.id ;
        this.props.onSubmit(err, values);
      }
    });
  };

  render() {
    let result = this.props.result;
    const { getFieldDecorator } = this.props.form;
    const { TextArea } = Input;
    return (
      <Form className={styles.mainForm} {...formItemLayout} onSubmit={this.handleSubmit}>
        <Form.Item style={{display:"none"}} {...formItemLayout} label="id：">
          {getFieldDecorator('id', {
            initialValue: result.id,
            rules: [{ required: false}],
          })(<Input disabled={!!this.props.disabled}/>)}
        </Form.Item>
        <Form.Item label="应用名称">
          {getFieldDecorator('app_name', {
            initialValue: result.app_name,
            rules: [{ required: true, message: '请输入应用名称！' }],
          })(<Input disabled={!!this.props.disabled}/>)}
        </Form.Item>
        <Form.Item label="应用备注">
          {getFieldDecorator('remark', {
            initialValue: result.remark,
            rules: [{ required: false}],
          })(<TextArea disabled={!!this.props.disabled} rows={8}/>)}
        </Form.Item>
        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit" disabled={!!this.props.disabled}>
            提交
          </Button>
        </Form.Item>
      </Form>
    );
  }
}
