export default [
  // user
  {
    path: '/user',
    component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', name: 'login', component: './User/Login' },
      { path: '/user/register', name: 'register', component: './User/Register' },
      {
        path: '/user/register-result',
        name: 'register.result',
        component: './User/RegisterResult',
      },
      {
        component: '404',
      },
    ],
  },

  // app
  {
    path: '/',
    component: '../layouts/BasicLayout',
    Routes: ['src/pages/Authorized'],
    routes: [
      // 设备管理
      { path: '/', redirect: '/iot_equipment_manage/equipment', authority: ['S', 'A', 'U'] },
      // { path: '/', redirect: '/iot_equipment_manage/equipment' },
      {
        path: '/iot_equipment_manage',
        name: 'iot_equipment_manage',
        icon: 'desktop',
        routes: [
          {
            path: '/iot_equipment_manage/equipment',
            name: 'equipment',
            component: './iot_equipment_manage/equipment',
          },
          {
            path: '/iot_equipment_manage/deviceType',
            name: 'deviceType',
            component: './iot_equipment_manage/deviceType',
          },
          {
            path: '/iot_equipment_manage/deviceGroup',
            name: 'deviceGroup',
            component: './iot_equipment_manage/deviceGroup',
          },
        ],
      },
      //应用管理
      { path: '/', redirect: '/iot_app_manage/iotApp' },
      {
        path: '/iot_app_manage',
        name: 'iot_app_manage',
        icon: 'database',
        routes: [
          {
            path: '/iot_app_manage/iotApp',
            name: 'iotApp',
            component: './iot_app_manage/iotApp',
          },
        ],
      },
      //订阅管理
      { path: '/', redirect: '/iot_subscribe_manage/iotSubscribe' },
      {
        path: '/iot_subscribe_manage',
        name: 'iot_subscribe_manage',
        icon: 'snippets',
        routes: [
          {
            path: '/iot_subscribe_manage/iotSubscribe',
            name: 'iotSubscribe',
            component: './iot_subscribe_manage/iotSubscribe',
          },
          {
            path: '/iot_subscribe_manage/iotSubscribeType',
            name: 'iotSubscribeType',
            component: './iot_subscribe_manage/iotSubscribeType',
          },
          {
            path: '/iot_subscribe_manage/iotSubscribeGroup',
            name: 'iotSubscribeGroup',
            component: './iot_subscribe_manage/iotSubscribeGroup',
          },
        ],
      },
      //消息管理
      { path: '/', redirect: '/iot_message_manage/iotMessage' },
      {
        path: '/iot_message_manage',
        name: 'iot_message_manage',
        icon: 'table',
        routes: [
          {
            path: '/iot_message_manage/iotMessage',
            name: 'iotMessage',
            component: './iot_message_manage/iotMessage',
          },
        ],
      },
      //用户管理
      { path: '/', redirect: '/iot_user_manage/iotUser' },
      {
        path: '/iot_user_manage',
        name: 'iot_user_manage',
        icon: 'user',
        routes: [
          {
            path: '/iot_user_manage/iotUser',
            name: 'iotUser',
            component: './iot_user_manage/iotUser',
          },
        ],
      },
      // { path: '/', redirect: '/iot_test_pages/iotWarpperPage' },
      // {
      //   path: '/iot_test_pages',
      //   name: 'iot_test_pages',
      //   icon: 'question-circle', 
      //   routes: [
      //     {
      //       path: '/iot_test_pages/iotWarpperPage',
      //       name: 'iotWarpperPage',
      //       component: './iot_test_pages/iotWarpperPage',
      //     },
      //   ],
      // },
      {
        component: '404',
      },
    ],
  },
];
